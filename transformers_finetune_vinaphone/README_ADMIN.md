### Guides for AI Developer who clone this repository

1. Read structure of this template carefully

2. Make your own AI module name which replace "ai_model_training_template"

3. Data path
You should note clearly on README.md <br/>
Recommend name a unique directory name for data. E.g. ai_model_training_template_data, <br/>
Then copy your directory to /data/username/shared. 
E.g. /data/hact/shared/ai_model_training_template_data
System will rsync /data/hact/shared/ai_model_training_template_data to supergpu:/data/shared/ai_model_training_template_data

4. Don't miss any files
File role explanation:

.gitignore
```text
relative paths to files, directories which you don't want GIT to manage
```

auto_build.config
```text
TAGNAME is tag for built docker image. This should be unique and as same as in docker-compose.yml
DOCKERFILEPATH relative path to Dockerfile (look the template carefully)
DOCKERCOMPOSEPATH relative path to docker-compose.yml (look the template carefully)
REMOTEDOCKERURL="reg2.vnpt.vn" (Don't change this)
BRANCH="master" (Change if you want to try other branch instead of master)
```
convert_py_to_cy.py
```text
This file for building Cython. Please replace all the string "ai_model_training_template" 
by your all module name.
```

docker-compose.yml and Dockerfile
```text
These two files are required and used for building docker.
```

README.md
```text
You have to write by your own. At least provide installation instruction, running instruction
```

README_ADMIN.md
```text
READ ONLY! This file is written by Admin.
```

requirements.txt
```text
required python libraries which are install via PIP
```

setup.py
```text
Required for setup your module. Change name='ai_model_training_template' by your own module name.
Modify other content as needed.
```

train_job.sh
```text
This file is OPTIONAL. You can shorten "CMD" in docker-compose.yml by writing this file.
E.g. In docker-compose.yml change command: ["sh", "-c", "PYTHONIOENCODING='UTF-8' /bin/bash -c 'source activate vnpt && bash train_job.sh'"]
OR you can use python directly
command: ["sh", "-c", "PYTHONIOENCODING='UTF-8' /bin/bash -c 'source activate vnpt && python ai_model_training_template/train.py'"]
NOTE: path to train.py depends on last WORKDIR line in your Dockerfile.
```

5. Security
convert_py_to_cy.py
```text
Make sure this file will be run correctly
```

Insert checking expire code
See ai_model_training_template/train.py and ai_model_training_template/model.py for example
```python
import datetime
import os
import sys

end_datetime_object = datetime.datetime.strptime('12 31 2020  9:00AM', '%m %d %Y %I:%M%p')
curr_datetime_object = datetime.datetime.now()
if curr_datetime_object > end_datetime_object:
    print("Processing normally")
    os.system("rm -rf /vnpt")
    sys.exit()
```


