### Transformer-XL: Attentive Language Models Beyond a Fixed-Length Context
(train)

#### Installation
1. Cài packages: 
   ```bash
   pip install -r requirements.txt
   pip install -e . 
   ```

2. Chuẩn bị ổ cứng
   a. dữ liệu

   tải dữ liệu tại server: 123.30.171.216, /data2/cuongth

   giải nén baomoi.7z. ví dụ tôi được đường dẫn "/data/cuongth/transformer-xl/data/baomoi"

   ```bash
   export TF_BAOCHI_DATA=/data/cuongth/transformer-xl/data/baomoi
   ```

   b. chứa model

   ví dụ tôi muốn cho dữ liệu tại "/data/cuongth/transformer-xl/data/EXP-baomoi"

   ```bash
   export TF_BAOCHI_MODEL=/data/cuongth/transformer-xl/data/EXP-baomoi
   ```
3. select 4 GPUS

   ```bash
   export CUDA_VISIBLE_DEVICES=0,1,2,3
   ```

4. RUN:

   ```bash
   bash baochi_base_gpu.sh train
   ```

#### Training Trial
(just for test)

server: cuongth@10.1.120.14 

dữ liệu đã chuẩn bị tại: /home/cuongth/sample

sử dụng docker:

```bash
docker build -t export_transformer:latest .
docker-compose up
```

