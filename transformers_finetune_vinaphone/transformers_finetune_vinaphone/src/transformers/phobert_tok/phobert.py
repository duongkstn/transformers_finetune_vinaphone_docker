import warnings
import os
from pyvi import ViTokenizer
import argparse
import torch
from fairseq.data.encoders.fastbpe import fastBPE
from fairseq.data import Dictionary
from transformers.tokenization_utils import AddedToken
import logging
from collections import OrderedDict
logger = logging.getLogger(__name__)


class PhobertTokenizer:
    def __init__(self, model_path):
        self.model_path = model_path
        self.bpe_path = os.path.join(self.model_path, 'bpe.codes')
        self.vocab_path = os.path.join(self.model_path, 'dict.txt')
        self.output_hidden_states = True
        self.cls_token_id = 0
        self.bos_token_id = 0
        self.pad_token_id = 1
        self.sep_token_id = 2
        self.eos_token_id = 2
        self.unk_token_id = 3

        self.cls_token = '<s>'
        self.bos_token = '<s>'
        self.pad_token = '<pad>'
        self.sep_token = '</s>'
        self.eos_token = '</s>'
        self.unk_token = '<unk>'
        self.mask_token = '<mask>'

        self._cls_token = AddedToken(self.cls_token, rstrip=False, lstrip=False, single_word=False, normalized=True)
        self._bos_token = AddedToken(self.bos_token, rstrip=False, lstrip=False, single_word=False, normalized=True)
        self._pad_token = AddedToken(self.pad_token, rstrip=False, lstrip=False, single_word=False, normalized=True)
        self._sep_token = AddedToken(self.sep_token, rstrip=False, lstrip=False, single_word=False, normalized=True)
        self._eos_token = AddedToken(self.eos_token, rstrip=False, lstrip=False, single_word=False, normalized=True)
        self._unk_token = AddedToken(self.unk_token, rstrip=False, lstrip=False, single_word=False, normalized=True)
        self._mask_token = AddedToken(self.mask_token, rstrip=False, lstrip=True, single_word=False, normalized=True)
        self.init_model()
        self.added_tokens_encoder = {}
        self.added_tokens_decoder = {}
        self.model_max_length = 512
        self.unique_no_split_tokens = ['</s>', '<mask>', '<pad>', '<s>', '<unk>']
        self.special_tokens_map_extended = {'bos_token': AddedToken("<s>", rstrip=False, lstrip=False, single_word=False, normalized=True),
                                            'eos_token': AddedToken("</s>", rstrip=False, lstrip=False, single_word=False, normalized=True),
                                            'unk_token': AddedToken("<unk>", rstrip=False, lstrip=False, single_word=False, normalized=True),
                                            'sep_token': AddedToken("</s>", rstrip=False, lstrip=False, single_word=False, normalized=True),
                                            'pad_token': AddedToken("<pad>", rstrip=False, lstrip=False, single_word=False, normalized=True),
                                            'cls_token': AddedToken("<s>", rstrip=False, lstrip=False, single_word=False, normalized=True),
                                            'mask_token': AddedToken("<mask>", rstrip=False, lstrip=True, single_word=False, normalized=True)}
        self.special_tokens_map = {'bos_token': '<s>', 'eos_token': '</s>', 'unk_token': '<unk>', 'sep_token': '</s>', 'pad_token': '<pad>', 'cls_token': '<s>', 'mask_token': '<mask>'}
        self.pretrained_init_configuration = {}
        self.padding_side = 'right'
        self.pad_token_type_id = 0
        self.model_input_names = ['attention_mask']
        self.errors = 'replace'
        self._additional_special_tokens = []
        self.verbose = True
        self.add_prefix_space = False

    def init_model(self):
        args = argparse.Namespace(bpe_codes=self.bpe_path)
        self.bpe = fastBPE(args)
        self.vocab = Dictionary()
        self.vocab.add_from_file(self.vocab_path)
        self.encoder = self.vocab.indices
        self.decoder = {v: k for k, v in self.encoder.items()}

        self.mask_token_id = len(self.encoder)
        self.encoder[self.mask_token] = self.mask_token_id

    def encode(self, sent, truncation=False, max_length=0, **kwargs):  # list of (pooling_layer, pooling_strategy)
        # max_length: include special tokens
        # print('max_lennnnnn', max_length)
        line = ViTokenizer.tokenize(sent)
        subwords = '<s> ' + self.bpe.encode(line) + ' </s>'
        input_ids = self.vocab.encode_line(subwords, append_eos=False, add_if_not_exist=False).long()
        # assert input_ids[0] == 0 and input_ids[-1] == 2
        num_tokens_to_remove = len(input_ids) - max_length
        # print('kikiki', len(input_ids))
        # print('kkkkk', input_ids)
        # print('numnum', num_tokens_to_remove)
        if truncation and num_tokens_to_remove > 0:
            # print(input_ids[1:-1][:-num_tokens_to_remove])
            # print(input_ids[0], input_ids[-1])
            # exit(0)
            # input_ids = input_ids[0] + input_ids[1:-1][:-num_tokens_to_remove] + input_ids[-1]
            input_ids = torch.cat((input_ids[0].view(-1), input_ids[1:-1][:-num_tokens_to_remove], input_ids[-1].view(-1)))
            # input_ids = pad_sequences(input_ids, maxlen=max_length, dtype="long", truncating="post")
        # print('input_idsssssssss', input_ids, input_ids.size())
        return input_ids

    def decode(self, token_ids):
        assert len(token_ids) == 1
        return self.vocab.symbols[token_ids[0]]

    def tokenize(self, sent):
        line = ViTokenizer.tokenize(sent)
        return self.bpe.encode(line).split()

    def _convert_token_to_id(self, token):
        """ Converts a token (str) in an id using the vocab. """
        return self.encoder.get(token, self.encoder.get(self.unk_token))

    def _convert_id_to_token(self, index):
        """Converts an index (integer) in a token (str) using the vocab."""
        return self.decoder.get(index)

    def convert_tokens_to_ids(self, tokens):
        """
        Converts a token string (or a sequence of tokens) in a single integer id (or a sequence of ids), using the
        vocabulary.

        Args:
            token (:obj:`str` or :obj:`List[str]`): One or several token(s) to convert to token id(s).

        Returns:
            :obj:`int` or :obj:`List[int]`: The token id or list of token ids.
        """
        if tokens is None:
            return None
        if isinstance(tokens, str):
            return self._convert_token_to_id_with_added_voc(tokens)
        ids = []
        for token in tokens:
            ids.append(self._convert_token_to_id_with_added_voc(token))
        return ids

    def _convert_token_to_id_with_added_voc(self, token):
        if token is None:
            return None
        if token in self.added_tokens_encoder:
            return self.added_tokens_encoder[token]
        return self._convert_token_to_id(token)

    def get_special_tokens_mask(
        self, token_ids_0, token_ids_1 = None, already_has_special_tokens = False
    ):
        """
        Retrieves sequence ids from a token list that has no special tokens added. This method is called when adding
        special tokens using the tokenizer ``prepare_for_model`` method.

        Args:
            token_ids_0 (:obj:`List[int]`):
                List of ids.
            token_ids_1 (:obj:`List[int]`, `optional`, defaults to :obj:`None`):
                Optional second list of IDs for sequence pairs.
            already_has_special_tokens (:obj:`bool`, `optional`, defaults to :obj:`False`):
                Set to True if the token list is already formatted with special tokens for the model

        Returns:
            :obj:`List[int]`: A list of integers in the range [0, 1]: 1 for a special token, 0 for a sequence token.
        """
        if already_has_special_tokens:
            if token_ids_1 is not None:
                raise ValueError(
                    "You should not supply a second sequence if the provided sequence of "
                    "ids is already formatted with special tokens for the model."
                )
            return list(map(lambda x: 1 if x in [self.sep_token_id, self.cls_token_id] else 0, token_ids_0))

        if token_ids_1 is None:
            return [1] + ([0] * len(token_ids_0)) + [1]
        return [1] + ([0] * len(token_ids_0)) + [1, 1] + ([0] * len(token_ids_1)) + [1]

    @property
    def vocab_size(self):
        return len(self.encoder)

    def __len__(self):
        """
        Size of the full vocabulary with the added tokens.
        """
        return self.vocab_size + len(self.added_tokens_encoder)

    @property
    def max_len(self):
        """
        :obj:`int`: **Deprecated** Kept here for backward compatibility. Now renamed to :obj:`model_max_length` to
        avoid ambiguity.
        """
        warnings.warn(
            "The `max_len` attribute has been deprecated and will be removed in a future version, use `model_max_length` instead.",
            FutureWarning,
        )
        return self.model_max_length

    def __call__(self, text, truncation=False, max_length=0, **kwargs):
        # print('_CAAAAAAAAAAAAAAAAAAA', max_length, truncation)
        rslt = [self.encode(sent, truncation=truncation, max_length=max_length) for sent in text]
        return rslt

    @property
    def is_fast(self):
        return False

    @property
    def all_special_tokens(self):
        """
        :obj:`List[str]`: All the special tokens (:obj:`'<unk>'`, :obj:`'<cls>'`, etc.) mapped to class attributes.

        Convert tokens of :obj:`tokenizers.AddedToken` type to string.
        """
        all_toks = [str(s) for s in self.all_special_tokens_extended]
        return all_toks

    @property
    def all_special_tokens_extended(self):
        """
        :obj:`List[Union[str, tokenizers.AddedToken]]`: All the special tokens (:obj:`'<unk>'`, :obj:`'<cls>'`, etc.)
        mapped to class attributes.

        Don't convert tokens of :obj:`tokenizers.AddedToken` type to string so they can be used to control more finely
        how special tokens are tokenized.
        """
        all_toks = []
        set_attr = self.special_tokens_map_extended
        for attr_value in set_attr.values():
            all_toks = all_toks + (list(attr_value) if isinstance(attr_value, (list, tuple)) else [attr_value])
        all_toks = list(OrderedDict.fromkeys(all_toks))
        return all_toks

    @property
    def all_special_ids(self):
        """
        :obj:`List[int]`: List the ids of the special tokens(:obj:`'<unk>'`, :obj:`'<cls>'`, etc.) mapped to class
        attributes.
        """
        all_toks = self.all_special_tokens
        all_ids = self.convert_tokens_to_ids(all_toks)
        return all_ids

    @property
    def additional_special_tokens_ids(self):
        """
        :obj:`List[int]`: Ids of all the additional special tokens in the vocabulary.
        Log an error if used while not having been set.
        """
        return self.convert_tokens_to_ids(self.additional_special_tokens)

    @property
    def additional_special_tokens(self):
        """
        :obj:`List[str]`: All the additional special tokens you may want to use. Log an error if used while not having
        been set.
        """
        if self._additional_special_tokens is None and self.verbose:
            logger.error("Using additional_special_tokens, but it is not set yet.")
            return None
        return [str(tok) for tok in self._additional_special_tokens]

    def num_special_tokens_to_add(self, pair= False):
        """
        Returns the number of added tokens when encoding a sequence with special tokens.

        .. note::
            This encodes a dummy input and checks the number of added tokens, and is therefore not efficient. Do not
            put this inside your training loop.

        Args:
            pair (:obj:`bool`, `optional`, defaults to :obj:`False`):
                Whether the number of added tokens should be computed in the case of a sequence pair or a single
                sequence.

        Returns:
            :obj:`int`: Number of special tokens added to sequences.
        """
        token_ids_0 = []
        token_ids_1 = []
        return len(self.build_inputs_with_special_tokens(token_ids_0, token_ids_1 if pair else None))

    def build_inputs_with_special_tokens(
            self, token_ids_0, token_ids_1= None
    ):
        """
        Build model inputs from a sequence or a pair of sequence for sequence classification tasks
        by concatenating and adding special tokens.
        A RoBERTa sequence has the following format:

        - single sequence: ``<s> X </s>``
        - pair of sequences: ``<s> A </s></s> B </s>``

        Args:
            token_ids_0 (:obj:`List[int]`):
                List of IDs to which the special tokens will be added
            token_ids_1 (:obj:`List[int]`, `optional`, defaults to :obj:`None`):
                Optional second list of IDs for sequence pairs.

        Returns:
            :obj:`List[int]`: list of `input IDs <../glossary.html#input-ids>`__ with the appropriate special tokens.
        """
        if token_ids_1 is None:
            return [self.cls_token_id] + token_ids_0 + [self.sep_token_id]
        cls = [self.cls_token_id]
        sep = [self.sep_token_id]
        return cls + token_ids_0 + sep + sep + token_ids_1 + sep