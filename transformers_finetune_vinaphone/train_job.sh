#!/bin/bash
ls
python /env_transformers/transformers_finetune_vinaphone/examples/language-modeling/run_language_modeling.py \
    --output_dir=$OUTPUT_DIR \
    --model_type=phobert \
    --model_name_or_path=$MODLE_NAME_OR_PATH \
    --line_by_line \
    --do_train \
    --train_data_file=/env_transformers/transformers_finetune_vinaphone/examples/language-modeling/vinaphone_text_data_not_tokenized_cleaned_final.txt \
    --overwrite_output_dir \
    --mlm \
    --save_steps $SAVE_STEPS  \
    --logging_steps 40000 \
    --save_total_limit $SAVE_TOTAL_LIMITS  \
    --per_gpu_train_batch_size $PER_GPU_TRAIN_BATCH_SIZE  \
    --block_size=$BLOCK_SIZE \
    --num_train_epochs $NUM_TRAIN_EPOCHS \


## test in local ##
#python ./transformers_finetune_vinaphone/transformers-master/examples/language-modeling/run_language_modeling.py \
#    --output_dir=/media/user/Data/phobert_base_output \
#    --model_type=phobert \
#    --model_name_or_path=/media/user/Data/phobert_base \
#    --line_by_line \
#    --do_train \
#    --train_data_file=./transformers_finetune_vinaphone/transformers-master/examples/language-modeling/vinaphone_text_data_not_tokenized_cleaned_final.txt \
#    --overwrite_output_dir \
#    --mlm \
#    --save_steps 60000 \
#    --logging_steps 60000 \
#    --save_total_limit 20 \
#    --per_gpu_train_batch_size 16 \
#    --block_size=258
