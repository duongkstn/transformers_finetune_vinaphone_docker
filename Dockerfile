FROM nvidia/cuda:10.0-cudnn7-devel-ubuntu16.04
LABEL maintainer="NGUYEN HAI SY"

# Environment
ENV LANG=en_US.utf8
ENV PATH=$PATH:/usr/local/cuda-10.0/bin
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-10.0/lib64
ENV LANG C.UTF-8

# Variables
#ENV OUTPUT_DIR=/data/duongdn/phobert_base_output
#ENV MODLE_NAME_OR_PATH=/data/duongdn/phobert_base
#ENV BLOCK_SIZE=258
#ENV PER_GPU_TRAIN_BATCH_SIZE=16
#ENV SAVE_TOTAL_LIMITS=20
#ENV SAVE_STEPS=60000

ENV CUDA_VISIBLE_DEVICES=0,1,2,3
# Install basic packages and miscellaneous dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    cmake \
    wget \
    curl \
    bzip2 \
    vim \
    ffmpeg \
    unzip \
    alien \
    libaio1\
    libsm6 libxext6 libxrender-dev\
    git \
    && curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \
    && bash /tmp/miniconda.sh -bfp /usr/local \
    && rm -rf /tmp/miniconda.sh \
    && apt-get clean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*
RUN apt update -y
# Create env
RUN conda clean --all --yes
RUN conda create -n env_transformers python=3.6
Run echo "source activate env_transformers" > ~/.bashrc
ENV PATH /opt/conda/envs/env_transformers/bin:$PATH
# run
COPY transformers_finetune_vinaphone/conda_install.sh /conda_install.sh
Run /bin/bash -c  "source activate env_transformers && sh conda_install.sh"
COPY transformers_finetune_vinaphone/requirements.txt /requirements.txt
Run /bin/bash -c  "source activate env_transformers && pip install -r requirements.txt"
ADD transformers_finetune_vinaphone /env_transformers
WORKDIR /env_transformers/transformers_finetune_vinaphone
RUN /bin/bash -c "source activate env_transformers && pip install ."
WORKDIR /env_transformers
#Run /bin/bash -c  "source activate env_transformers && \
#    python3 setup.py build_ext --inplace && \
#    python3 convert_py_to_cy.py build_ext --inplace && \
#    pip install -e ."
#RUN /bin/bash -c "source activate env_transformers && pip install -e ."
